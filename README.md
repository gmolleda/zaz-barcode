ZAZ BarCode

Extension for insert barcode in LibreOffice documents, with Python, of course.

Barcodes provided

* code39
* code128
* ean
* ean13
* ean8
* gs1
* gtin
* isbn
* isbn10
* isbn13
* issn
* jan
* pzn
* upc
* upca
* qrcode


Thanks!

https://gitlab.com/mauriciobaeza/zaz

https://github.com/WhyNotHugo/python-barcode

https://github.com/lincolnloop/python-qrcode


This extension have a cost of maintenance of 1 euro every year.

BCH: `1RPLWHJW34p7pMQV1ft4x7eWhAYw69Dsb`

BTC: `3Fe4JuADrAK8Qs7GDAxbSXR8E54avwZJLW`


* [See the wiki](https://gitlab.com/mauriciobaeza/zaz-barcode/wikis/home)
* [Mira la wiki](https://gitlab.com/mauriciobaeza/zaz-barcode/wikis/home_es)
